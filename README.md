This project wont be translated in english, my sentences and variables are in french, you must translate ( or not ) in your language if you want to understand most of my explanations.

This project is an imagined and [python](https://python.org) coded by [Desparias](https://gitlab.com/Desparias).

<details><summary>Project stats</summary>

|            | Iterative version (1.0) | Recurcive version (2.0) |
| :--------- | ----------------------- | ----------------------- |
| length     | N/A lines| N/A lines |
| display    | terminal | terminal |  
| clean code | N/A | N/A |  
| size       | N/A | N/A |  
| files      | 1 | 1 |
</details>

Any help or advise is welcome.  

This project which is a game is F2P. Both version are available and playable.  
You just need python on your computer.  
 
You can tell/ask anything you want to help/understand this project.

Thanks for reading, and maybe playing.
