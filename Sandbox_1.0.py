grille = [ [ "•" for _ in range(5) ] for _ in range(5) ]

def décalage(grille, direction):
    if 1 <= direction <= 10:  # ←
        for i in range(5):
            temp_grille = list()
            reste = 0
            for j in range(5):
                if grille[i][j] != "•":
                    temp_grille.append(grille[i][j])
                else:
                    reste += 1
            for _ in range(reste): temp_grille.append("•")
            grille[i] = temp_grille
        return grille

    if 10 <= direction <= 20:  # →
        for i in range(5):
            temp_grille = list()
            reste = 0
            for j in range(5):
                if grille[i][j] != "•":
                    temp_grille.append(grille[i][j])
                else:
                    reste += 1
                    
            grille[i] = ["•" for _ in range(reste)] + temp_grille
        return grille

    if 20 <= direction <= 30:  # ↑
        for j in range(5):
            temp_grille = list()
            reste = 0
            for i in range(5):
                if grille[i][j] != "•":
                    temp_grille.append(grille[i][j])
                else:
                    reste += 1

            for _ in range(reste): temp_grille.append("•")
            
            for l in range(5): grille[l][j] = temp_grille[l]
        return grille

    if 30 <= direction <= 40:  # ↓
        for j in range(5):
            temp_grille = list()
            reste = 0
            for i in range(5):
                if grille[i][j] != "•":
                    temp_grille.append(grille[i][j])
                else:
                    reste += 1
            
            temp_grille = ["•" for _ in range(reste) ] + temp_grille
            
            for l in range(5): grille[l][j] = temp_grille[l]
        return grille
    else:
        return décalage(grille, randint(1, 41))

def affichage(grille):
    for n in range(5):
        print("+---+---+---+---+---+")
        print("|", end=" ")
        for m in range(5):
            print(grille[n][m], end="")
            print(" | ", end="")
        print()
    print("+---+---+---+---+---+\n")

def choix_joueur(joueur):

    ti = int(input("Vertical: "))-1
    tj = int(input("Horizontal: "))-1
    if grille[ti][tj] == "•":
        grille[ti][tj] = joueur

def verif(grille, joueur):

    for n in range(1):
        for j in range(5):
            if grille[j][0+n] == joueur and \
                grille[j][1+n] == joueur and \
                grille[j][2+n] == joueur and \
                grille[j][3+n] == joueur :
                return True

            elif grille[0+n][j] == joueur and \
                grille[1+n][j] == joueur and \
                grille[2+n][j] == joueur and \
                grille[3+n][j] == joueur:
                return True
    return False

from random import randint ; from os import system
joueur = "O" ; affichage(grille)

while not verif(grille, joueur):

    try: system('CLS')
    except: system('clear')

    grille = décalage(grille, randint(1, 41))
    affichage(grille)
    
    if joueur == "X": joueur = "O"
    else: joueur = "X"

    choix_joueur(joueur)
print(f"\nLe Joueur {joueur}")
